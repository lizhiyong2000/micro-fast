package com.micro.fast.upms.dao;

import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UpmsPermissionMapper extends SsmMapper<UpmsPermission,Integer> {
    @Override
    int deleteByPrimaryKey(Integer id);

    @Override
    int insert(UpmsPermission record);

    @Override
    int insertSelective(UpmsPermission record);

    @Override
    UpmsPermission selectByPrimaryKey(Integer id);

    @Override
    int updateByPrimaryKeySelective(UpmsPermission record);

    @Override
    int updateByPrimaryKey(UpmsPermission record);

    @Override
    List<UpmsPermission> selectByCondition(UpmsPermission record);

    @Override
    int deleteByPrimaryKeys(List<Integer> integers);

    List<UpmsPermission> selectByUserIdAndType(@Param("userId") Integer userId,@Param("pType") Integer pType,@Param("type") Integer type);
}
